import numpy as np
np.random.seed(1001)

import os
import shutil

import IPython
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from tqdm import tqdm_notebook
from sklearn.model_selection import StratifiedKFold
from scipy.io import wavfile


matplotlib.style.use('ggplot')
import IPython.display as ipd  # To play sound in the notebook
fname = 'MIR-1K_for_MIREX/Wavfile/' + 'abjones_1_01.wav'

fs, data = wavfile.read(fname)
# print(np.unique(data[:,1], axis=0))
y = np.unique(data[:, 1], axis=0)
# print(y.shape, data.shape[0], fs)
# print(data[:5])
# print(np.unique(data[:, 1]))
dataFrame = {'wav_data': data[:, 0], 'considering_param': data[:, 1]}
df = pd.DataFrame(data=dataFrame)
# print(df.head(n=15))
considering_param_group = df.groupby('considering_param').count()
plot = considering_param_group.unstack().reindex(
    considering_param_group.unstack().sum(axis=1)).sort_value().index).plot(
        kind='bar', stacked=True,figsize=(16,10))
