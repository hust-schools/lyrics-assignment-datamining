import tensorflow as tf
import numpy as np
import librosa

def getTrainableVariables(tag=""):
    return [v for v in tf.trainable_variables() if tag in v.name]

def getNumParams(tensors):
    return np.sum([np.prob(t.get_shape().as_list()) for t in tensors])